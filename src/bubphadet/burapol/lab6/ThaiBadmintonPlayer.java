package bubphadet.burapol.lab6;

import bubphadet.burapol.lab5.BadmintonPlayer;
/**
* BadmintonPlayer is a subclass of BadmintonPlayer and has additional variables as equipment that is set to "ลูกขนไก่"
*
* @author  Burapol Bubphadet
* @version 1.0
* @since   2018-02-17
*/

import bubphadet.burapol.lab5.Gender;

public class ThaiBadmintonPlayer extends BadmintonPlayer {
	protected static String equipment;
	{
		equipment = "ÅÙ¡¢¹ä¡è";
	}

	public ThaiBadmintonPlayer(String name, double weight, double height, Gender gender, String birthdate,
			double racketLength, int worldRanking) {
		super(name, weight, height, gender, null, birthdate, racketLength, worldRanking);
	}

	protected String nationality = "Thai";

	public String getEquipment() {
		return equipment;
	}

	public void setEquipment(String equipment) {
		ThaiBadmintonPlayer.equipment = equipment;
	}

	@Override
	public void play() {
		System.out.println(name + " hits a " + super.getEquipment());
		System.out.println(name + " hits a " + this.getEquipment());

	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

}
