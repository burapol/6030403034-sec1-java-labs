package bubphadet.burapol.lab6;
/**
* Author is a program for print data from user.
*
* @author  Burapol Bubphadet
* @version 1.0
* @since   2018-02-17
*/

public class Author {
	private String name;
	private String email;
	private char gender;

	public Author(String name, String email, char gender) {
		super();
		this.name = name;
		this.email = email;
		this.gender = gender;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public char getGender() {
		return gender;
	}

	public void setGender(char gender) {
		this.gender = gender;
	}

	@Override
	public String toString() {
		return "Author [name=" + name + ", email=" + email + ", gender=" + gender + "]";
	}
	
	public String toStringNames() {
		return name ;
	}
	
	

	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

}
