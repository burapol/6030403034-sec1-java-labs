package bubphadet.burapol.lab6;
/**
* book1(Array) is a program for print data from user.
*
* @author  Burapol Bubphadet
* @version 1.0
* @since   2018-02-17
*/

import java.util.Arrays;

public class book1 {
	public String name;
	public Author[] author;
	public double price;
	public int qty;
	{
		qty = 0;
	}

	public book1(String name, Author[] author, double price) {
		super();
		this.name = name;
		this.author = author;
		this.price = price;
	}

	public book1(String name, Author[] author, double price, int qty) {
		super();
		this.name = name;
		this.author = author;
		this.price = price;
		this.qty = qty;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Author[] getAuthor() {
		return author;
	}

	public void setAuthor(Author[] author) {
		this.author = author;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public int getQty() {
		return qty;
	}

	public void setQty(int qty) {
		this.qty = qty;
	}

	@Override
	public String toString() {
		return "Book [name=" + name + ", " + System.lineSeparator() + "author=" + Arrays.toString(author) + ", price="
				+ price + ", qty=" + qty + "]";
	}

	public String getAuthorNanes() {
		int x = author.length;
		for (int i = 0; i < x; i++) {
			System.out.print(author[i].getName() + ", ");
		}
		return " ";
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

}
