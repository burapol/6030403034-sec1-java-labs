package bubphadet.burapol.lab6;
/**
* Book is a program for print data from user.
*
* @author  Burapol Bubphadet
* @version 1.0
* @since   2018-02-17
*/


public class Book {
	private String name;
	private Author author;
	private double price;
	private int qty;
	{
		qty = 0;
	}

	public Book(String name, Author author, double price) {
		super();
		this.name = name;
		this.author = author;
		this.price = price;
	}

	public Book(String name, Author author, double price, int qty) {
		super();
		this.name = name;
		this.author = author;
		this.price = price;
		this.qty = qty;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Author getAuthor() {
		return author;
	}

	public void setAuthor(Author author) {
		this.author = author;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public int getQty() {
		return qty;
	}

	public void setQty(int qty) {
		this.qty = qty;
	}
	

	@Override
	public String toString() {
		return "Book [name=" + name + ", author=" + author + ", price=" + price + ", qty=" + qty + "]";
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

}
