package bubphadet.burapol.lab10;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.geom.Rectangle2D;
import java.util.Random;

import javax.swing.JPanel;

public class MovingPongGamePanelV3 extends JPanel implements Runnable, KeyListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1517386834598685393L;

	protected MovingBall movingBall;
	private Thread running;
	private Random rand;
	private int ballR = 20;
	protected MovablePongPaddle movableRightPad;
	protected MovablePongPaddle movableLeftPad;
	protected Rectangle2D.Double box;
	protected Integer player1Score;
	protected Integer player2Score;
	protected int[] directionValue = { -3, -2, -1, 1, 2, 3 };
	private int stringLength;

	public MovingPongGamePanelV3() {
		super();

		addKeyListener(this);
		setFocusable(true);

		setBackground(Color.BLACK);

		// initialize the pads
		movableLeftPad = new MovablePongPaddle(0, SimpleGameWindow.HEIGHT / 2 - PongPaddle.HEIGHT / 2, PongPaddle.WIDTH,
				PongPaddle.HEIGHT);
		movableRightPad = new MovablePongPaddle(SimpleGameWindow.WIDTH - PongPaddle.WIDTH,
				SimpleGameWindow.HEIGHT / 2 - PongPaddle.HEIGHT / 2, PongPaddle.WIDTH, PongPaddle.HEIGHT);
		resetBall();

		// initialize the ball
		box = new Rectangle2D.Double(0, 0, SimpleGameWindow.WIDTH, SimpleGameWindow.HEIGHT);

		// set the player scores
		player1Score = 0;
		player2Score = 0;

		running = new Thread(this);
		running.start();
	}

	private void resetBall() {
		rand = new Random();
		int someInt = rand.nextInt(3) - 1;
		if (someInt == 0) {
			someInt = someInt + 1;
		}
		movingBall = new MovingBall((SimpleGameWindow.WIDTH / 2) - 20, (SimpleGameWindow.HEIGHT / 2) - 20, ballR, 1*someInt,
				1*someInt);
	}

	@Override
	public void run() {

		while (true) {

			moveBall();
			repaint();
			this.getToolkit().sync(); // to flush the graphic buffer
			if (player1Score == 10 || player2Score == 10) {
				break;
			}

			// Delay
			try {
				// try to adjust the number here to have a smooth
				// running ball on your machine
				Thread.sleep(15);
			} catch (InterruptedException ex) {
				System.err.println(ex.getStackTrace());
			}
		}
	}

	// update position of the ball
	private void moveBall() {

		int diameter = (ballR * 2);
		movingBall.x = movingBall.x + movingBall.getVelX();
		movingBall.y = movingBall.y + movingBall.getVelY();

		if (movingBall.y >= SimpleGameWindow.HEIGHT - diameter || movingBall.y <= 0) {
			movingBall.setVelY(-movingBall.getVelY());
		}
		if (movingBall.intersects(movableLeftPad) || movingBall.intersects(movableRightPad)) {
			movingBall.setVelX(-movingBall.getVelX());
		}
		movingBall.move();

		if (movingBall.x >= SimpleGameWindow.WIDTH + diameter) {
			resetBall();
			player1Score++;
		}
		if (movingBall.x < 0 - diameter) {
			resetBall();
			player2Score++;
		}

	}

	public void paintComponent(Graphics g) {
		super.paintComponent(g);

		Graphics2D g2 = (Graphics2D) g;

		g2.setColor(Color.WHITE);

		// draw the middle line
		g2.drawLine(SimpleGameWindow.WIDTH / 2, 0, SimpleGameWindow.WIDTH / 2, SimpleGameWindow.HEIGHT);

		// draw line on the left
		g2.drawLine(movableLeftPad.getW(), 0, movableLeftPad.getW(), SimpleGameWindow.HEIGHT);

		// draw line on the right
		g2.drawLine(SimpleGameWindow.WIDTH - movableRightPad.getW(), 0, SimpleGameWindow.WIDTH - movableRightPad.getW(),
				SimpleGameWindow.HEIGHT);

		// Draw the score
		g2.setFont(new Font(Font.SERIF, Font.BOLD, 48));
		g2.drawString(player1Score.toString(), SimpleGameWindow.WIDTH / 4, SimpleGameWindow.HEIGHT / 5);
		g2.drawString(player2Score.toString(), 3 * SimpleGameWindow.WIDTH / 4, SimpleGameWindow.HEIGHT / 5);

		// Draw the paddles
		g2.fill(movableLeftPad);
		g2.fill(movableRightPad);

		// draw the ball
		g2.fill(movingBall);

		// draw the box
		g2.draw(box);

		if (player1Score == 10) {
			stringLength = g2.getFontMetrics().stringWidth("Player 1 Wins.");
			g2.drawString("Player 1 Wins.", (SimpleGameWindow.WIDTH - stringLength) / 2, SimpleGameWindow.HEIGHT - 325);
		}
		if (player2Score == 10) {
			stringLength = g2.getFontMetrics().stringWidth("Player 2 Wins.");
			g2.drawString("Player 2 Wins.", (SimpleGameWindow.WIDTH - stringLength) / 2, SimpleGameWindow.HEIGHT - 325);
		}

	}

	@Override
	public void keyPressed(KeyEvent e) {
		int keyPress = e.getKeyCode();
		if (keyPress == KeyEvent.VK_UP) {
			movableRightPad.moveUp();
		} else if (keyPress == KeyEvent.VK_DOWN) {
			movableRightPad.moveDown();
		} else if (keyPress == KeyEvent.VK_W) {
			movableLeftPad.moveUp();
		} else if (keyPress == KeyEvent.VK_S) {
			movableLeftPad.moveDown();
		}

		repaint();

	}

	@Override
	public void keyReleased(KeyEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void keyTyped(KeyEvent e) {
		// TODO Auto-generated method stub

	}
}