package bubphadet.burapol.lab5;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
/**
* Footballer is a subclass of Athlete and has additional variables as sport that is set to "American Football", position, and team.
*
* @author  Burapol Bubphadet
* @version 1.0
* @since   2018-02-04
*/
public class Footballer implements Playable,Moveable {

	private String name;
	private double weight;
	private double height;
	private Gender gender;
	private String nationality;
	private LocalDate birthdate;
	private String position; //string 
	private String team; //string

	DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy"); //format LocalDate

	@Override
	public String toString() {
		return name + ", " + weight + "kg, " + height + "m, " + gender + ", " + nationality + ", " + birthdate
				+ ", American Football,  " + position + ", " + team; //toString format
	}

	public Footballer(String name, double weight, double height, Gender gender, String nationality, String birthdate,
			String position, String team) {
		super();
		this.name = name;
		this.weight = weight;
		this.height = height;
		this.gender = gender;
		this.nationality = nationality;
		this.birthdate = LocalDate.parse(birthdate, formatter);
		this.position = position;
		this.team = team;
	}
	
	public String playSport() {
		System.out.println(name + "is good at" + " American football");
		return name;
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public double getWeight() {
		return weight;
	}

	public void setWeight(double weight) {
		this.weight = weight;
	}

	public double getHeight() {
		return height;
	}

	public void setHeight(double height) {
		this.height = height;
	}

	public Gender getGender() {
		return gender;
	}

	public void setGender(Gender gender) {
		this.gender = gender;
	}

	public String getNationality() {
		return nationality;
	}

	public void setNationality(String nationality) {
		this.nationality = nationality;
	}

	public LocalDate getBirthdate() {
		return birthdate;
	}

	public void setBirthdate(String birthdate) {
		this.birthdate = LocalDate.parse(birthdate, formatter);
	}

	public String getPosition() {
		return position;
	}

	public void setPosition(String position) {
		this.position = position;
	}

	public String getTeam() {
		return team;
	}

	public void setTeam(String team) {
		this.team = team;
	}

	public static void main(String[] args) {

	}
	public void play() {
		System.out.println(name + " throws a touchdown.");
		
	}

	@Override
	public void move() {
		System.out.println(name + " moves down the football field.");
		
	}

}
