package bubphadet.burapol.lab5;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
/**
* Athlete is a class to describe an athlete with the private properties as name, weight, height, gender, nationality, birthdate.
*
* @author  Burapol Bubphadet
* @version 1.0
* @since   2018-02-04
*/
public class Athlete {

	private String name;
	private double weight;
	private double height;
	private Gender gender;
	private String nationality;
	private LocalDate birthdate;

	DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");

	@Override 
	public String toString() { //to string fomat 
		return "Athlete [" + name + ", " + weight + "kg, " + height + "m, " + gender + ", " + nationality + ", "
				+ birthdate + "]";
	}

	public Athlete(String name, double weight, double height, Gender gender, String nationality, String birthdate) { 
		super(); 
		this.name = name;
		this.weight = weight;
		this.height = height;
		this.gender = gender;
		this.nationality = nationality;
		this.birthdate = LocalDate.parse(birthdate, formatter);
	}
	
	
	public String playSport() {
		System.out.println(name + " is good at sport");
		return name;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public double getWeight() {
		return weight;
	}

	public void setWeight(double weight) {
		this.weight = weight;
	}

	public double getHeight() {
		return height;
	}

	public void setHeight(double height) {
		this.height = height;
	}

	public Gender getGender() {
		return gender;
	}

	public void setGender(Gender gender) {
		this.gender = gender;
	}

	public String getNationality() {
		return nationality;
	}

	public void setNationality(String nationality) {
		this.nationality = nationality;
	}

	public LocalDate getBirthdate() {
		return birthdate;
	}

	public void setBirthdate(String birthdate) {
		this.birthdate = LocalDate.parse(birthdate);
	}

	public static void main(String[] args) {

	}

}
