package bubphadet.burapol.lab5;

public class ThailandGoTournament extends Competition {
	protected String Winner;



	public ThailandGoTournament(String name, String date, String place, String description) {
		super(name, date, place, description);
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

	@Override
	public void setDescriptionAndRules() {
		// TODO Auto-generated method stub
		System.out.println("==== Begin: Description and Rules =============");
		System.out.println("The competition is open for all students and people");
		System.out.println("The competition is divided into four catagories");
		System.out.println("1. High Dan (3 Dan and above)");
		System.out.println("2. Low Dan (1-2 Dan)");
		System.out.println("3. High Kyu (1-4 Kyu)");
		System.out.println("4. Low Kyu (5-8 Kyu)");
		System.out.println("==== End: Description and Rules =============");
	}

	public String getWinner() {
		return Winner;
	}



	@Override
	public String toString() {
		return name + " was held on " + date + " in " + place + "." + System.lineSeparator()
		+ "Same winners are " + Winner ;
	}

	public void setWinner(String winner) {
		Winner = winner;
	}


}
