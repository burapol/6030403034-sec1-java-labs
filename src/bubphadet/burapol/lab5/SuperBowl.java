package bubphadet.burapol.lab5;

public class SuperBowl extends Competition {

	protected String AFCTeam;
	protected String NFCTeam;
	protected String winningteam;


	public SuperBowl(String name, String date, String place, String description, String aFCTeam, String nFCTeam,
			String winningteam) {
		super(name, date, place, description);
		AFCTeam = aFCTeam;
		NFCTeam = nFCTeam;
		this.winningteam = winningteam;
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

	@Override
	public void setDescriptionAndRules() {
		System.out.println("==== Begin: Description and Rules =============");
		System.out.println("SuperBowlLII is played between the champions of National Football Conference (NFC) ");
		System.out.println("and the American Football Conference (AFC)");
		System.out.println("The game plays in four quarters while each quarter takes about 15 minutes.");
		System.out.println("==== End: Description and Rules =============");
	}

	@Override
	public String toString() {
		return name + "(" + description + ") " + " was held on " + date + "in " + place 
				+ System.lineSeparator() + "It was the game between " + AFCTeam + " vs " + NFCTeam + "." +  System.lineSeparator()
				+ "The winner was " + winningteam + "." ;
	}

}
