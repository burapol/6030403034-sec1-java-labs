package bubphadet.burapol.lab5;
/**
* Gender is an enum type which has values as either MALE or FEMALE
*
* @author  Burapol Bubphadet
* @version 1.0
* @since   2018-02-04
*/

public enum Gender {
	MALE, FEMALE

}
