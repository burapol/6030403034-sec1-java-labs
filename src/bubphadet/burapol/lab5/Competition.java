package bubphadet.burapol.lab5;

public abstract class Competition {
	protected String name;
	protected String date;
	protected String place;
	protected String description;

	public Competition(String name, String date, String place, String description) {
		super();
		this.name = name;
		this.date = date;
		this.place = place;
		this.description = description;
	}

	public abstract void setDescriptionAndRules();

}
