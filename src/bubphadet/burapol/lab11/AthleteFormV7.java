package bubphadet.burapol.lab11;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.io.File;

import javax.swing.JColorChooser;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.KeyStroke;
import javax.swing.SwingUtilities;

import com.sun.glass.events.KeyEvent;

import bubphadet.burapol.lab9.AthleteFormV6;

/**
 * AthleteFormV8 which extends from AthleteFormV7
 * 
 *
 * @author Burapol Bubphadet
 * @version 1.0
 * @since 2018-05-02
 * 
 */

public class AthleteFormV7 extends AthleteFormV6 {
	/**
	 * 
	 */
	private static final long serialVersionUID = 3843391674366844105L;
	protected KeyStroke ctrlN, ctrlO, ctrlS, ctrlQ;
	protected JPanel setkey;
	protected JFileChooser open_filechooser,save_filechooser;

	public AthleteFormV7(String title) {
		super(title);
	}

	protected void addComponents() {
		super.addComponents();
		Menu_file.setMnemonic(KeyEvent.VK_F);
		new_Item.setMnemonic(KeyEvent.VK_N);
		open_Item.setMnemonic(KeyEvent.VK_O);
		save_Item.setMnemonic(KeyEvent.VK_S);
		exit_Item.setMnemonic(KeyEvent.VK_X);
		KeyStroke ctrlN = KeyStroke.getKeyStroke("control N");
		new_Item.setAccelerator(ctrlN);
		KeyStroke ctrlO = KeyStroke.getKeyStroke("control O");
		open_Item.setAccelerator(ctrlO);
		KeyStroke ctrlS = KeyStroke.getKeyStroke("control S");
		save_Item.setAccelerator(ctrlS);
		KeyStroke ctrlQ = KeyStroke.getKeyStroke("control Q");
		exit_Item.setAccelerator(ctrlQ);
	}

	@Override
	public void actionPerformed(ActionEvent event) {
		super.actionPerformed(event);
		Object cmd = event.getSource();
		if (cmd == open_Item) {
			open_filechooser();

		} else if (cmd == save_Item) {
			save_filechooser();

		} else if (cmd == exit_Item) {
			System.exit(0);
		} else if (cmd == custom) {
			colorChooser();
		}

	}

	protected void open_filechooser() {
		open_filechooser = new JFileChooser();
		open_filechooser.setDialogTitle("Open");
		int selectedButton = open_filechooser.showDialog(null, "Open");
		if (selectedButton == JFileChooser.APPROVE_OPTION) {
			File selectedFile = open_filechooser.getSelectedFile();
			String fileName = open_filechooser.getName(selectedFile);
			JOptionPane.showMessageDialog(null, "Open" + ": " + fileName);
		} else {
			JOptionPane.showMessageDialog(null, "Open" + " command cancelled by user.");
		}
	}
	
	public void save_filechooser() {
		save_filechooser = new JFileChooser();
		save_filechooser.setDialogTitle("Save");
		int selectedButton = save_filechooser.showDialog(null, "Save");
		if (selectedButton == JFileChooser.APPROVE_OPTION) {
			File selectedFile = save_filechooser.getSelectedFile();
			String fileName = save_filechooser.getName(selectedFile);
			JOptionPane.showMessageDialog(null, "Save" + ": " + fileName);
		} else {
			JOptionPane.showMessageDialog(null, "Saving" + " command cancelled by user.");
		}
	}

	public void colorChooser() {
		JColorChooser clr = new JColorChooser();
		Color color = clr.showDialog(null, "Choose Color", getBackground());
		if (color != null) {
			competitionText.setBackground(color);
		}
	}

	protected void addListeners() {
		super.addListeners();
		open_Item.addActionListener(this);
		save_Item.addActionListener(this);
		exit_Item.addActionListener(this);
		custom.addActionListener(this);
	}

	public static void createAndShowGUI() {
		AthleteFormV7 AthleteForm7 = new AthleteFormV7("Athlete Form V7");
		AthleteForm7.addComponents();
		AthleteForm7.setFrameFeatures();
	}

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});
	}

}
