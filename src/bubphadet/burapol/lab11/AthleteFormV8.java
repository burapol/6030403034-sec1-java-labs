package bubphadet.burapol.lab11;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;

import bubphadet.burapol.lab5.Gender;
import bubphadet.burapol.lab5.Athlete;

/**
 * AthleteFormV8 which extends from AthleteFormV7
 * 
 *
 * @author Burapol Bubphadet
 * @version 1.0
 * @since 2018-05-02
 * 
 */
public class AthleteFormV8 extends AthleteFormV7 implements ActionListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7033445627901094363L;
	protected JMenu Menu_data;
	protected JMenuItem display_Item, sort_Item, search_Item, remove_Item;
	protected ArrayList<Athlete> athlete_List = new ArrayList<Athlete>();
	protected Gender gender;

	public AthleteFormV8(String title) {
		super(title);
	}

	/**
	 * 
	 */

	public void addAthlete() {
		if (genderButton == "Male") {
			gender = Gender.MALE;
		} else {
			gender = Gender.FEMALE;
		}
		athlete_List.add(new Athlete(nameField.getText(), Double.parseDouble(weightField.getText()),
				Double.parseDouble(heightField.getText()), gender, nationalityField.getText(),
				birthdateField.getText()));
		System.out.println(athlete_List);
	}

	public void displayAthlete() {
		String msg = "";
		for (int i = 0; i < athlete_List.size(); i++) {
			msg += (i + 1) + ". " + athlete_List.get(i).toString() + "\n";
		}
		JOptionPane.showMessageDialog(null, msg, "Message", JOptionPane.INFORMATION_MESSAGE);
	}

	@Override
	public void actionPerformed(ActionEvent event) {
		super.actionPerformed(event);
		Object src = event.getSource();
		if (src == okBotton) {
			addAthlete();
		} else if (src == display_Item) {
			displayAthlete();
		} else if (src == sort_Item) {
			Collections.sort(athlete_List, new HeightComparator());
			displayAthlete();
		}
	}

	public class HeightComparator implements Comparator<Athlete> {
		@Override
		public int compare(Athlete paerson1, Athlete person2) {
			double athleteHeight1 = paerson1.getHeight();
			double athleteHeight2 = person2.getHeight();
			if (athleteHeight1 > athleteHeight2)
				return 1;
			else if (athleteHeight1 < athleteHeight2)
				return -1;
			return 0;
		}
	}

	protected void addComponents() {
		super.addComponents();
		Menu_data.add(display_Item);
		Menu_data.add(sort_Item);
		Menu_data.add(search_Item);
		Menu_data.add(remove_Item);
		menu_Bar.add(Menu_data);
		setJMenuBar(menu_Bar);
	}

	public void addMenuItem() {
		Menu_data = new JMenu("Data");
		display_Item = new JMenuItem("Display");
		sort_Item = new JMenuItem("Sort");
		search_Item = new JMenuItem("Search");
		remove_Item = new JMenuItem("Remove");
	}

	public void addListeners() {
		super.addListeners();
		addMenuItem();
		display_Item.addActionListener(this);
		sort_Item.addActionListener(this);
	}

	public static void createAndShowGUI() {
		AthleteFormV8 AthleteForm8 = new AthleteFormV8("Athlete Form V8");
		AthleteForm8.addComponents();
		AthleteForm8.setFrameFeatures();
	}

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});
	}

}
