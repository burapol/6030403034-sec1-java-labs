package bubphadet.burapol.lab7.advanced;

import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Toolkit;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

/**
 * MySimpleWindow (advanced version) for lab9
 * 
 * MySimpleWindow is program for creat UI. UI has title and
 * okbotton,cancelbotton.
 *
 * @author Burapol Bubphadet
 * @version 1.0
 * @since 2018-04-03
 * 
 * 
 * 
 */

public class MySimpleWindow extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	protected JButton okBotton;
	protected JButton cancelBotton;
	protected JPanel mainPanel;

	public MySimpleWindow(String title) {
		super(title);
	}

	protected void addComponents() {
		cancelBotton = new JButton("Cancel");
		okBotton = new JButton("OK");
		mainPanel = new JPanel(new FlowLayout());
		mainPanel.add(cancelBotton);
		mainPanel.add(okBotton);
		this.setContentPane(mainPanel);
	}

	protected void setFrameFeatures() {
		pack();
		Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
		int width = this.getWidth();
		int height = this.getHeight();
		int x = (dim.width - width) / 2;
		int y = (dim.height - height) / 2;
		setLocation(x, y);
		setDefaultLookAndFeelDecorated(true);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setVisible(true);

	}

	public static void createAndShowGUI() {
		MySimpleWindow msw = new MySimpleWindow("My Simple Window");
		msw.addComponents();
		msw.setFrameFeatures();
	}

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});
	}

}
