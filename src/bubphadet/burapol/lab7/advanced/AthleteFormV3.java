package bubphadet.burapol.lab7.advanced;

import java.awt.BorderLayout;
import java.awt.Dimension;

import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

/**
 * AthleteFormV3 (advanced version) for lab9
 *
 * @author Burapol Bubphadet
 * @version 1.0
 * @since 2018-04-03
 * 
 * 
 * 
 */

public class AthleteFormV3 extends AthleteFormV2 {

	protected JMenuBar menu_Bar;
	protected JMenu Menu_file, Menu_config, Item_color, Item_size;
	protected JMenuItem new_Item, open_Item, save_Item, exit_Item;
	protected JLabel combo_type;
	protected JComboBox<String> type_Combo;
	protected JPanel type_Panel;

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public AthleteFormV3(String title) {
		super(title);
	}

	protected void addComponents() {
		super.addComponents();
		menu_Bar = new JMenuBar();
		Menu_file = new JMenu("File");
		Menu_config = new JMenu("Config");
		new_Item = new JMenuItem("New");
		open_Item = new JMenuItem("Open");
		save_Item = new JMenuItem("Save");
		exit_Item = new JMenuItem("Exit");
		Item_color = new JMenu("Color");
		Item_size = new JMenu("Size");
		Menu_file.add(new_Item);
		Menu_file.add(open_Item);
		Menu_file.add(save_Item);
		Menu_file.add(exit_Item);
		Menu_config.add(Item_color);
		Menu_config.add(Item_size);
		menu_Bar.add(Menu_file);
		menu_Bar.add(Menu_config);
		setJMenuBar(menu_Bar);
		combo_type = new JLabel("Type : ");
		type_Combo = new JComboBox<String>();
		type_Combo.setPreferredSize(new Dimension(210, 25));
		type_Combo.addItem("Badminton player");
		type_Combo.addItem("Boxer");
		type_Combo.addItem("Footballer");
		type_Combo.setSelectedIndex(1);
		type_Combo.setEditable(true);
		type_Panel = new JPanel(new BorderLayout());
		type_Panel.add(type_Combo, BorderLayout.EAST);
		type_Panel.add(combo_type, BorderLayout.WEST);
		competitionPanel.add(type_Panel, BorderLayout.SOUTH);
		this.setContentPane(Panel101);

	}

	public static void createAndShowGUI() {
		AthleteFormV3 AthleteForm3 = new AthleteFormV3("Athlete Form V3");
		AthleteForm3.addComponents();
		AthleteForm3.setFrameFeatures();
	}

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});
	}
}
