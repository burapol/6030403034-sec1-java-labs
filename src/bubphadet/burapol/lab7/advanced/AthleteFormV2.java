package bubphadet.burapol.lab7.advanced;

import java.awt.BorderLayout;
import java.awt.GridLayout;

import javax.swing.ButtonGroup;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.SwingUtilities;

/**
 * AthleteFormV2 (advanced version) for lab9
 * 
 *
 * @author Burapol Bubphadet
 * @version 1.0
 * @since 2018-04-03
 * 
 * 
 * 
 */

public class AthleteFormV2 extends AthleteFormV1 {

	protected JLabel gender, competition;
	protected JTextArea competitionText;
	protected JRadioButton maleRadiobuttonButton, femaleRadiobuttonButton;
	protected ButtonGroup Groupbutton;
	protected JPanel genderPanel, mfButtPanel, mainGenderPanel, competitionPanel, Panel102, Panel2;
	protected JScrollPane competitionScroll;

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public AthleteFormV2(String title) {
		super(title);
	}

	protected void addComponents() {
		super.addComponents();
		gender = new JLabel("Gender : ");
		maleRadiobuttonButton = new JRadioButton("Male", true);
		femaleRadiobuttonButton = new JRadioButton("Female");
		Groupbutton = new ButtonGroup();
		Groupbutton.add(maleRadiobuttonButton);
		Groupbutton.add(femaleRadiobuttonButton);
		genderPanel = new JPanel(new GridLayout(1, 2));
		genderPanel.add(gender);
		mfButtPanel = new JPanel(new GridLayout(1, 2));
		mfButtPanel.add(maleRadiobuttonButton);
		mfButtPanel.add(femaleRadiobuttonButton);
		genderPanel.add(mfButtPanel);
		
		
		competition = new JLabel("Competition : ");
		competitionText = new JTextArea(2, 35);
		competitionScroll = new JScrollPane(competitionText);
		competitionText.setLineWrap(true);
		competitionPanel = new JPanel(new BorderLayout());
		competitionPanel.add(competition, BorderLayout.NORTH);
		competitionPanel.add(competitionScroll, BorderLayout.CENTER);
		competitionText.setText("Competed in the 31st national championship");
		Panel102 = new JPanel(new BorderLayout());
		Panel102.add(genderPanel, BorderLayout.NORTH);
		Panel102.add(competitionPanel, BorderLayout.CENTER);
		Panel2 = new JPanel();
		Panel2.add(Panel102);
		Panel1.add(Panel2, BorderLayout.CENTER);
		Panel101.add(Panel1);
		this.setContentPane(Panel101);
	}

	public static void createAndShowGUI() {
		AthleteFormV2 AthleteForm2 = new AthleteFormV2("Athlete Form V2");
		AthleteForm2.addComponents();
		AthleteForm2.setFrameFeatures();
	}

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});
	}

}
