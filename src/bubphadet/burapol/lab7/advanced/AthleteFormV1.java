package bubphadet.burapol.lab7.advanced;

import java.awt.BorderLayout;
import java.awt.GridLayout;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;

/**
 * AthleteFormV1 (advanced version) for lab9
 * 
 *
 * @author Burapol Bubphadet
 * @version 1.0
 * @since 2018-04-03
 * 
 */

public class AthleteFormV1 extends MySimpleWindow {

	private int textLenght = 15;

	protected JLabel nameLabel, birthdateLabel, weightLabel, heightLabel, nationalityLabel;
	protected JTextField nameField, birthdateField, weightField, heightField, nationalityField;
	protected JPanel panel, namePanel, birthPanel, weightPanel, heightPanel, nationPanel, Panel1, Panel101;

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public AthleteFormV1(String title) {
		super(title);
	}

	protected void addComponents() {
		super.addComponents();
		nameLabel = new JLabel("Name : ");
		birthdateLabel = new JLabel("Birthdate : ");
		weightLabel = new JLabel("Weight(kg.) : ");
		heightLabel = new JLabel("Height(metre) : ");
		nationalityLabel = new JLabel("Nationality : ");
		nameField = new JTextField(textLenght);
		birthdateField = new JTextField(textLenght);
		weightField = new JTextField(textLenght);
		heightField = new JTextField(textLenght);
		nationalityField = new JTextField(textLenght);
		birthdateField.setToolTipText("ex. 22.02.2000");
		namePanel = new JPanel(new GridLayout(1, 2));
		namePanel.add(nameLabel);
		namePanel.add(nameField);
		birthPanel = new JPanel(new GridLayout(1, 2));
		birthPanel.add(birthdateLabel);
		birthPanel.add(birthdateField);
		weightPanel = new JPanel(new GridLayout(1, 2));
		weightPanel.add(weightLabel);
		weightPanel.add(weightField);
		heightPanel = new JPanel(new GridLayout(1, 2));
		heightPanel.add(heightLabel);
		heightPanel.add(heightField);
		nationPanel = new JPanel(new GridLayout(1, 2));
		nationPanel.add(nationalityLabel);
		nationPanel.add(nationalityField);
		panel = new JPanel(new GridLayout(5, 1));
		panel.add(namePanel);
		panel.add(birthPanel);
		panel.add(weightPanel);
		panel.add(heightPanel);
		panel.add(nationPanel);
		Panel1 = new JPanel(new BorderLayout());
		Panel1.add(panel, BorderLayout.NORTH);
		Panel1.add(mainPanel, BorderLayout.SOUTH);
		Panel101 = new JPanel();
		Panel101.add(Panel1);
		this.setContentPane(Panel101);
	}

	public static void createAndShowGUI() {
		AthleteFormV1 athleteForm1 = new AthleteFormV1("Athlete Form V1");
		athleteForm1.addComponents();
		athleteForm1.setFrameFeatures();
	}

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});
	}
}
