package bubphadet.burapol.lab7;

/**
 * AthleteFormV3 which extends from AthleteFormV2. 
 *
 * @author Burapol Bubphadet
 * @version 1.0
 * @since 2018-03-12
 * 
 * 
 * 
 */
import java.awt.*;

import javax.swing.*;

public class AthleteFormV3 extends AthleteFormV2 {

	private static final long serialVersionUID = 1L;

	public AthleteFormV3(String title) {
		super(title);
	}

	/**
	 * addComboBox is void method for add Combobox to panel.
	 * 
	 * 
	 * @param pane
	 * @param label
	 * @param comboBox
	 * @param texts
	 */
	protected void addComboBox(JPanel pane, JLabel label, JComboBox<String> comboBox, String... texts) { //
		Panel athleteTypePane = new Panel();
		athleteTypePane.setLayout(new GridLayout(1, 2));

		for (String text : texts) {
			comboBox.addItem(text);
		}
		comboBox.setEditable(true);

		athleteTypePane.add(label);
		athleteTypePane.add(comboBox);
		pane.add(athleteTypePane);
	}
	
	

	/**
	 * addMenuBar is void method for add menubar to panel.
	 * 
	 * 
	 * 
	 */
	protected void addMenuBar() { // creat new component for AthleteV3.
		JMenuBar menuBar = new JMenuBar();// menu bar
		JMenu menuFile = new JMenu("File");
		JMenuItem item_New = new JMenuItem("New");
		JMenuItem item_Open = new JMenuItem("Open");
		JMenuItem item_Save = new JMenuItem("Save");
		JMenuItem item_Exit = new JMenuItem("Exit");
		menuFile.add(item_New);
		menuFile.add(item_Open);
		menuFile.add(item_Save);
		menuFile.add(item_Exit);
		menuBar.add(menuFile);
		JMenu ConfigBar = new JMenu("Config"); // config bar
		JMenuItem item_Color = new JMenuItem("Color");
		JMenuItem item_Size = new JMenuItem("Size");
		ConfigBar.add(item_Color);
		ConfigBar.add(item_Size);
		menuBar.add(ConfigBar);

		this.setJMenuBar(menuBar);
	}

	protected void addComponents() { // add component from AthleteV2 and new component from AthleteV3.
		super.addComponents();
		JComboBox<String> athleteTypeCombo = new JComboBox<String>();
		JLabel athleteTypeLabel = new JLabel("Type");
		addComboBox(mainPanel, athleteTypeLabel, athleteTypeCombo, "Badminton Player", "Boxer", "Football");
		add(mainPanel, BorderLayout.NORTH);
	}

	public static void createAndShowGUI() {
		AthleteFormV3 AthleteForm3 = new AthleteFormV3("Athlete Form V3");
		AthleteForm3.addMenuBar();
		AthleteForm3.addComponents();
		AthleteForm3.setFrameFeatures();
	}

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});
	}
}