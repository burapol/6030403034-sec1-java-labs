package bubphadet.burapol.lab7;

import java.awt.BorderLayout;
import java.awt.GridLayout;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;

/**
 * AthleteFormV1 which extends from MySimpleWindow AthleteFormV1 is UI . UI has
 * Label,Button and Field.
 * 
 *
 * @author Burapol Bubphadet
 * @version 1.0
 * @since 2018-03-12
 * 
 * 
 * 
 */

public class AthleteFormV1 extends MySimpleWindow {

	private int textLenght = 15;

	private static final long serialVersionUID = 1L;

	public AthleteFormV1(String title) {
		super(title);
	}

	/**
	 * addComponents is void method for creat component to add in panel.
	 * 
	 */

	protected void addComponents() {
		super.addComponents();
		mainPanel.setLayout(new BoxLayout(mainPanel, BoxLayout.Y_AXIS));
		JLabel nameLabel = new JLabel("Name:");
		JLabel birthdateLabel = new JLabel("Birthdate:");
		JLabel weightLabel = new JLabel("Weight (kg.):");
		JLabel heightLabel = new JLabel("Height (metre):");
		JLabel nationalityLabel = new JLabel("Nationality:");
		JTextField nameField = new JTextField(textLenght);
		JTextField birthdateField = new JTextField(textLenght);
		JTextField weightField = new JTextField(textLenght);
		JTextField heightField = new JTextField(textLenght);
		JTextField nationalityField = new JTextField(textLenght);
		addTextLabelAndField(mainPanel, nameLabel, nameField);
		addTextLabelAndField(mainPanel, birthdateLabel, birthdateField);
		birthdateField.setToolTipText("ex. 22.02.2000");
		addTextLabelAndField(mainPanel, weightLabel, weightField);
		addTextLabelAndField(mainPanel, heightLabel, heightField);
		addTextLabelAndField(mainPanel, nationalityLabel, nationalityField);
		add(mainPanel, BorderLayout.NORTH);
	}

	/**
	 * addTextLabelAndField is method for add component to panel
	 * 
	 * @param panel
	 * @param label
	 * @param textField
	 */
	protected void addTextLabelAndField(JPanel panel, JLabel label, JTextField textField) {
		JPanel textAndFiedPane = new JPanel();
		textAndFiedPane.setLayout(new GridLayout(1, 2));
		textAndFiedPane.add(label);
		textAndFiedPane.add(textField);
		textAndFiedPane.setBorder(BorderFactory.createEmptyBorder(2, 0, 2, 0));
		panel.add(textAndFiedPane);
	}

	public static void createAndShowGUI() {
		AthleteFormV1 athleteForm1 = new AthleteFormV1("Athlete Form V1");
		athleteForm1.addComponents();
		athleteForm1.setFrameFeatures();
	}

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});
	}
}