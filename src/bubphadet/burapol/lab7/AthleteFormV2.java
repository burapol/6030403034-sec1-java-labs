package bubphadet.burapol.lab7;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.GridLayout;

import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextArea;
import javax.swing.SwingUtilities;

/**
 * AthleteFormV2 which extends from AthleteFormV1. Label,Bottin and Field.
 * 
 *
 * @author Burapol Bubphadet
 * @version 1.0
 * @since 2018-03-12
 * 
 * 
 * 
 */

public class AthleteFormV2 extends AthleteFormV1 {

	private static final long serialVersionUID = 1L;

	public AthleteFormV2(String title) {
		super(title);
	}

	protected void addComponents() {
		super.addComponents();
		JPanel labelAndRation = new JPanel();
		JPanel rationShow = new JPanel();
		JLabel labelShow = new JLabel("Gender: ");
		labelAndRation.setLayout(new GridLayout(1, 2));
		labelAndRation.add(labelShow);
		labelAndRation.add(rationShow);
		mainPanel.add(labelAndRation);
		JRadioButton radioButton1 = new JRadioButton("Male", true);
		JRadioButton radioButton2 = new JRadioButton("Female");
		rationShow.setLayout(new FlowLayout());
		rationShow.add(radioButton1);
		rationShow.add(radioButton2);
		ButtonGroup group = new ButtonGroup();
		group.add(radioButton1);
		group.add(radioButton2);
		labelAndRation.add(rationShow);
		JPanel textAreaPane = new JPanel();
		textAreaPane.setLayout(new GridLayout(2, 1));
		textAreaPane.setBorder(BorderFactory.createEmptyBorder(2, 0, 2, 0));
		JLabel competitionLabel = new JLabel("Competition: ");
		JTextArea textArea = new JTextArea(2, 35);
		textArea.setText("Competed in the 31st national championship");
		textAreaPane.add(competitionLabel);
		textAreaPane.add(textArea);
		mainPanel.add(textAreaPane);
		add(mainPanel, BorderLayout.NORTH);
	}

	public static void createAndShowGUI() {
		AthleteFormV2 AthleteForm2 = new AthleteFormV2("Athlete Form V2");
		AthleteForm2.addComponents();
		AthleteForm2.setFrameFeatures();
	}

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});
	}
}