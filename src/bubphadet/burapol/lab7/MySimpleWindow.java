package bubphadet.burapol.lab7;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Toolkit;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

/**
 * MySimpleWindow which extends from JFrame
 * 
 * MySimpleWindow is program for creat UI. UI has title and
 * okbotton,cancelbotton.
 *
 * @author Burapol Bubphadet
 * @version 1.0
 * @since 2018-03-12
 * 
 * 
 * 
 */

public class MySimpleWindow extends JFrame {

	private static final long serialVersionUID = 1L;

	protected JPanel mainPanel;
	protected JButton okBotton;
	protected JButton cancelBotton;

	public MySimpleWindow(String title) {
		super(title);
	}

	protected void addComponents() {
		mainPanel = new JPanel();
		setLayout(new BorderLayout());
		JPanel BottonPane = new JPanel();
		okBotton = new JButton("OK");
		cancelBotton = new JButton("Cancel");
		BottonPane.add(cancelBotton);
		BottonPane.add(okBotton);
		add(BottonPane, BorderLayout.SOUTH);
	}

	protected void setFrameFeatures() {
		pack();
		Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
		int w = getSize().width;
		int h = getSize().height;
		int x = (dim.width - w) / 2;
		int y = (dim.height - h) / 2;
		setLocation(x, y);
		setVisible(true);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

	}

	public static void createAndShowGUI() {
		MySimpleWindow msw = new MySimpleWindow("My Simple Window");
		msw.addComponents();
		msw.setFrameFeatures();
	}

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});
	}
}