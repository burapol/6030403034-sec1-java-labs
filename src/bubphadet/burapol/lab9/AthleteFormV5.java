package bubphadet.burapol.lab9;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.SwingUtilities;

/**
 * AthleteFormV5 which extends from AthleteFormV4 Actions are added to the menu
 * bar is available.
 *
 * @author Burapol Bubphadet
 * @version 1.0
 * @since 2018-04-03
 * 
 */

public class AthleteFormV5 extends AthleteFormV4 implements ActionListener {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	protected JMenu colorItem, sizeItem;
	protected JMenuItem blue, green, red, custom, Size_16, Size_20, Size_24, custom_Size;

	public AthleteFormV5(String title) {
		super(title);
	}

	private void handleColor(Color color) {
		nameField.setForeground(color);
		birthdateField.setForeground(color);
		weightField.setForeground(color);
		heightField.setForeground(color);
		nationalityField.setForeground(color);
		competitionText.setForeground(color);
	}

	private void handleSize(int size) {
		Font textFont = new Font(Font.SANS_SERIF, Font.BOLD, size);
		nameField.setFont(textFont);
		birthdateField.setFont(textFont);
		weightField.setFont(textFont);
		heightField.setFont(textFont);
		nationalityField.setFont(textFont);
		competitionText.setFont(textFont);
	}

	protected void configAdding() {
		colorItem = new JMenu("Color");
		sizeItem = new JMenu("Size");
		blue = new JMenuItem("Blue");
		green = new JMenuItem("Green");
		red = new JMenuItem("Red");
		custom = new JMenuItem("Custom...");
		Size_16 = new JMenuItem("16");
		Size_20 = new JMenuItem("20");
		Size_24 = new JMenuItem("24");
		custom_Size = new JMenuItem("Custom...");
	}

	protected void addComponents() {
		super.addComponents();
		Menu_config.removeAll();
		colorItem.add(blue);
		colorItem.add(green);
		colorItem.add(red);
		colorItem.add(custom);
		sizeItem.add(Size_16);
		sizeItem.add(Size_20);
		sizeItem.add(Size_24);
		sizeItem.add(custom_Size);
		Menu_config.add(colorItem);
		Menu_config.add(sizeItem);
	}

	protected void addListeners() {
		super.addListeners();
		configAdding();
		blue.addActionListener(this);
		green.addActionListener(this);
		red.addActionListener(this);
		Size_16.addActionListener(this);
		Size_20.addActionListener(this);
		Size_24.addActionListener(this);

	}

	@Override
	public void actionPerformed(ActionEvent event) {
		super.actionPerformed(event);
		Object src = event.getSource();
		if (src == blue) {
			handleColor(Color.BLUE);
		} else if (src == green) {
			handleColor(Color.GREEN);
		} else if (src == red) {
			handleColor(Color.RED);
		} else if (src == Size_16) {
			int size = Integer.valueOf(Size_16.getText());
			handleSize(size);
		} else if (src == Size_20) {
			int size = Integer.valueOf(Size_20.getText());
			handleSize(size);
		} else if (src == Size_24) {
			int size = Integer.valueOf(Size_24.getText());
			handleSize(size);
		}

	}

	public static void createAndShowGUI() {
		AthleteFormV5 AthleteForm5 = new AthleteFormV5("Athlete Form V5");
		AthleteForm5.addComponents();
		AthleteForm5.setFrameFeatures();
	}

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});
	}

}
