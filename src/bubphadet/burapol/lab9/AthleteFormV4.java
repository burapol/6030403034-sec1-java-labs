package bubphadet.burapol.lab9;

import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;

import bubphadet.burapol.lab7.advanced.AthleteFormV3;


/**
 * AthleteFormV4 which extends from AthleteFormV3 and implements from ActionListener, ItemListener
 * AthleteFormV4 To add a Listeners to  get the variable and displayed as showMessageDialog.
 * 
 *
 * @author Burapol Bubphadet
 * @version 1.0
 * @since 2018-04-03
 * 
 */

public class AthleteFormV4 extends AthleteFormV3 implements ActionListener, ItemListener {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	protected String genderButton;

	public AthleteFormV4(String title) {
		super(title);
	}

	private void handleOKButton() {
		if (maleRadiobuttonButton.isSelected()) {
			genderButton = maleRadiobuttonButton.getText();
		} else if (femaleRadiobuttonButton.isSelected()) {
			genderButton = femaleRadiobuttonButton.getText();
		}
		String message = "Name = " + nameField.getText() + ", Birthdate = " + birthdateField.getText() + ", Weight = "
				+ weightField.getText() + ", Height = " + heightField.getText() + ", Nation = "
				+ nationalityField.getText() + "\nGender = " + genderButton + "\nCompetition = "
				+ competitionText.getText() + "\nType = " + type_Combo.getSelectedItem();
		JOptionPane.showMessageDialog(null, message);

	}

	private void handleCancelButton() {
		nameField.setText("");
		birthdateField.setText("");
		weightField.setText("");
		heightField.setText("");
		nationalityField.setText("");
		competitionText.setText("");
		maleRadiobuttonButton.setSelected(true);
		type_Combo.setSelectedIndex(1);
	}

	private void handleGenderButton(String gender) {  
		Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
		int width = this.getWidth();
		int height = this.getHeight();
		int x = (dim.width - width) / 2;
		int y = (dim.height + height) / 2;
		JOptionPane showGender = new JOptionPane("Your gender type is now changed to " + gender);
		JDialog title = showGender.createDialog(null, "Gender info");
		title.setLocation(x, y + 20);
		title.setVisible(true);
	}

	@Override
	public void actionPerformed(ActionEvent event) {
		Object cmd = event.getSource();
		if (cmd == okBotton) {
			handleOKButton();
		} else if (cmd == cancelBotton) {
			handleCancelButton();
		} else if (cmd == type_Combo) {
			JComboBox<?> typeCombo = (JComboBox<?>) event.getSource();
			Object typeSelected = typeCombo.getSelectedItem();
			JOptionPane.showMessageDialog(null, "Your athlete type is changed to " + typeSelected);
		}

	}

	@Override
	public void itemStateChanged(ItemEvent e) {
		if (e.getStateChange() == ItemEvent.SELECTED) {
			Object click = e.getItemSelectable();
			if (click == maleRadiobuttonButton) {
				String gender = maleRadiobuttonButton.getText();
				handleGenderButton(gender);
			} else if (click == femaleRadiobuttonButton) {
				String gender = femaleRadiobuttonButton.getText();
				handleGenderButton(gender);
			}
		}
	}

	protected void addComponents() {
		super.addComponents();
		addListeners();
	}

	protected void addListeners() {
		okBotton.addActionListener(this);
		cancelBotton.addActionListener(this);
		type_Combo.addActionListener(this);
		maleRadiobuttonButton.addItemListener(this);
		femaleRadiobuttonButton.addItemListener(this);
	}

	public static void createAndShowGUI() {
		AthleteFormV4 AthleteForm4 = new AthleteFormV4("Athlete Form V4");
		AthleteForm4.addComponents();
		AthleteForm4.setFrameFeatures();
	}

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});
	}

}
