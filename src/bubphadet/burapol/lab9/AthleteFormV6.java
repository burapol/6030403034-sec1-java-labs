package bubphadet.burapol.lab9;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.net.URL;

import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.KeyStroke;
import javax.swing.SwingUtilities;

import com.sun.glass.events.KeyEvent;

/**
 * AthleteFormV6 which extends from AthleteFormV5 AthleteFormV6 Adding images
 * and icons to the component.
 * 
 *
 * @author Burapol Bubphadet
 * @version 1.0
 * @since 2018-04-03
 * 
 */
public class AthleteFormV6 extends AthleteFormV5  {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	protected JPanel Panel3, picturePanel;
	protected JLabel pictureLabel;
	protected URL file_imageBoxer, file_imageOpen, file_imageSave, file_imageQuit;
	protected ImageIcon image_Boxer, image_Open, image_Save, image_Quit;
	private int x = 25;

	public AthleteFormV6(String title) {
		super(title);
	}

	protected void addComponents() {
		super.addComponents();
		Panel3 = new JPanel(new BorderLayout());
		picturePanel = new JPanel(new BorderLayout());
		file_imageBoxer = this.getClass().getResource("mana.jpg");
		file_imageOpen = this.getClass().getResource("openIcon.png");
		file_imageSave = this.getClass().getResource("saveIcon.png");
		file_imageQuit = this.getClass().getResource("quitIcon.png");
		image_Boxer = new ImageIcon(file_imageBoxer);
		image_Open = new ImageIcon(file_imageOpen);
		image_Save = new ImageIcon(file_imageSave);
		image_Quit = new ImageIcon(file_imageQuit);
		open_Item.setIcon(image_Open);
		save_Item.setIcon(image_Save);
		exit_Item.setIcon(image_Quit);
		pictureLabel = new JLabel(image_Boxer);
		picturePanel.setBorder(BorderFactory.createEmptyBorder(x, x, x, x));
		picturePanel.add(pictureLabel);
		Panel3.add(picturePanel, BorderLayout.CENTER);
		Panel3.add(Panel101, BorderLayout.SOUTH);
		this.setContentPane(Panel3);
	}

	protected void addListeners() {
		super.addListeners();

	}

	public static void createAndShowGUI() {
		AthleteFormV6 AthleteForm6 = new AthleteFormV6("Athlete Form V6");
		AthleteForm6.addComponents();
		AthleteForm6.setFrameFeatures();
	}

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});
	}

}
