package bubphadet.burapol.lab2;

/**
 * 
 * @author burapol bubphadet
 *
 *date 19/01/2561
 * 
 *This program can find substring in string and count this string
 */

public class SubstringChecker {

	public static void main(String[] args) {
		if (args.length >= 2) {   //check agruments lenght

			int len = args.length;
			String substring = args[0];
			int count = 0;
			for (int i = 1; i < len; i++) {
				String string = args[i];
				if (string.indexOf(substring) != -1) {
					System.out.println("String " + i + " : " + string + " contains " + substring);
					count += 1; //if this string contains substring. it will count
				}
			}
			if (count > 0) {
				System.out.println("There are " + count + " string that contain " + substring);
			} else if (count == 0) {
				System.out.println("There are no strings that contain " + substring);
			}
		} else {
			System.err.println("SubstringChecker <sunstring> <str1> ... ");//error if args < 2
		}

	}
}
