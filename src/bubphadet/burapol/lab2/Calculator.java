package bubphadet.burapol.lab2;

/**
 * 
 * @author burapol bubphadet
 *
 *date 19/01/2561
 * 
 *This program can calculate two operand for find answer
 */

public class Calculator {

	public static void main(String[] args) {
		if (args.length >= 3) { // check args lenght
			double operand1 = Double.parseDouble(args[0]);
			double operand2 = Double.parseDouble(args[1]);
			String operator = args[2];
			
			if (operator.equals("+")) {
				double ans = operand1 + operand2;
				System.out.print(operand1 + " + " + operand2 + " = " + ans);
			} else if (operator.equals("-")) {
				double ans = operand1 - operand2;
				System.out.print(operand1 + " - " + operand2 + " = " + ans);
			} else if (operator.equals("*") || operator.equals(".classpath")) { // "*"classpath
				double ans = operand1 * operand2;
				System.out.print(operand1 + " * " + operand2 + " = " + ans);
			} else if (operator.equals("/")) {
				if (operand2 != 0) {
					double ans = operand1 / operand2;
					System.out.print(operand1 + " / " + operand2 + " = ");
					System.out.printf("%.2f", ans);

				} else if (operand2 == 0) { // if operand2 is zero print this
					System.out.println("The second operand cannot be zero");

				}
			}

		} else if (args.length != 3) {
			System.err.println("Calculator <operand1> <operand2> <operator>");

		}

	}

}
