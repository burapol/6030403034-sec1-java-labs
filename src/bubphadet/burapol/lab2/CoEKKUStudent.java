package bubphadet.burapol.lab2;

/**
 * 
 * @author burapol bubphadet
 *
 *date 19/01/2561
 * 
 *This program save the information about a CoE KKU student and find
 *different from the first CoE student's ID
 */
public class CoEKKUStudent {

	public static void main(String[] args) {
		if (args.length == 5) {
			String name = args[0];
			long student_id = (long) Double.parseDouble(args[1]); // convert string agrument to float agrument
			String gpa = args[2];
			String year = args[3];
			long income = (long) Double.parseDouble(args[4]);
			long diff = student_id - 3415717;// find different from the first CoE student's ID

			System.out.println(name + " has ID = " + student_id + " GPA = " + gpa + " year = " + year
					+ " parent's income = " + income);
			System.out.print("Your ID is different from the first CoE student's ID by " + diff);

		} else {
			System.err.print("CoEKKUStudent <name> <ID> <GPA> <academic year> <parents' income> ");
		}

	}

}
