package bubphadet.burapol.lab4;
/**
 * 
 * Burapol Bubphadet 603040303-4 sec1
 * 
 * 
 *Athlete class to run on WorldAthelete class main
 *
 * 
 */
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class Athlete {

	private String name;
	private double weight;
	private double height;
	private Gender gender;
	private String nationality;
	private LocalDate birthdate;

	DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");

	@Override 
	public String toString() { //to string fomat 
		return "Athlete [" + name + ", " + weight + "kg, " + height + "m, " + gender + ", " + nationality + ", "
				+ birthdate + "]";
	}

	public Athlete(String name, double weight, double height, Gender gender, String nationality, String birthdate) { 
		super(); 
		this.name = name;
		this.weight = weight;
		this.height = height;
		this.gender = gender;
		this.nationality = nationality;
		this.birthdate = LocalDate.parse(birthdate, formatter);
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public double getWeight() {
		return weight;
	}

	public void setWeight(double weight) {
		this.weight = weight;
	}

	public double getHeight() {
		return height;
	}

	public void setHeight(double height) {
		this.height = height;
	}

	public Gender getGender() {
		return gender;
	}

	public void setGender(Gender gender) {
		this.gender = gender;
	}

	public String getNationality() {
		return nationality;
	}

	public void setNationality(String nationality) {
		this.nationality = nationality;
	}

	public LocalDate getBirthdate() {
		return birthdate;
	}

	public void setBirthdate(String birthdate) {
		this.birthdate = LocalDate.parse(birthdate);
	}

	public static void main(String[] args) {

	}

}
