package bubphadet.burapol.lab4;
/**
 * 
 * Burapol Bubphadet 603040303-4 sec1
 * 
 * 
 *Class Boxer is a subclass of Athlete.
 *
 * 
 */
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class Boxer {

	private String name;
	private double weight;
	private double height;
	private Gender gender;
	private String nationality;
	private LocalDate birthdate;
	private String division;
	private String golveSize;

	DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");//format LocalDate

	@Override
	public String toString() {
		return name + ", " + weight + "kg, " + height + "m, " + gender + ", " + nationality + ", " + birthdate
				+ ", Boxing,  " + division + ", " + golveSize; //toString format
	}

	public Boxer(String name, double weight, double height, Gender gender, String nationality, String birthdate,
			String division, String golveSize) {
		super();
		this.name = name;
		this.weight = weight;
		this.height = height;
		this.gender = gender;
		this.nationality = nationality;
		this.birthdate = LocalDate.parse(birthdate, formatter);
		this.division = division;
		this.golveSize = golveSize;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public double getWeight() {
		return weight;
	}

	public void setWeight(double weight) {
		this.weight = weight;
	}

	public double getHeight() {
		return height;
	}

	public void setHeight(double height) {
		this.height = height;
	}

	public Gender getGender() {
		return gender;
	}

	public void setGender(Gender gender) {
		this.gender = gender;
	}

	public String getNationality() {
		return nationality;
	}

	public void setNationality(String nationality) {
		this.nationality = nationality;
	}

	public LocalDate getBirthdate() {
		return birthdate;
	}

	public void setBirthdate(String birthdate) {
		this.birthdate = LocalDate.parse(birthdate, formatter);
	}

	public String getDivision() {
		return division;
	}

	public void setDivision(String division) {
		this.division = division;
	}

	public String getGolveSize() {
		return golveSize;
	}

	public void setGolveSize(String golveSize) {
		this.golveSize = golveSize;
	}

	public static void main(String[] args) {

	}

}
