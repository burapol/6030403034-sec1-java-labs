package bubphadet.burapol.lab4;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;

/**
* BadmintonPlayer is a subclass of Athlete and has additional variables as sport that is set to "Badminton", racketLength, and worldRanking.
*
* @author  Burapol Bubphadet
* @version 1.0
* @since   2018-02-04
*/
public class BadmintonPlayer extends Athlete {

	private String name;
	private double weight;
	private double height;
	private Gender gender;
	private String nationality;
	private LocalDate birthdate;
	private static String sport;{
		sport = "Badminton";
	}
	private double racketLength;
	private int worldRanking;

	DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");

	@Override
	public String toString() {
		return name + ", " + weight + "kg, " + height + "m, " + gender + ", " + nationality + ", " + birthdate + ", "
				+ sport + ", " + racketLength + "cm, rank:" + worldRanking;
	}

	public BadmintonPlayer(String name, double weight, double height, Gender gender, String nationality,
			String birthdate, double racketLength, int worldRanking) {
		super(name,weight,height,gender,nationality,birthdate);
		this.name = name;
		this.weight = weight;
		this.height = height;
		this.gender = gender;
		this.nationality = nationality;
		this.birthdate = LocalDate.parse(birthdate, formatter);
		this.racketLength = racketLength;
		this.worldRanking = worldRanking;

	}
	
	public String playSport() {
		System.out.println(name + "is good at" + " Badminton");
		return name;
	}

	public static String getSport() {;
		return sport;
	}

	public static void setSport(String sport) {
		BadmintonPlayer.sport = sport;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public double getWeight() {
		return weight;
	}

	public void setWeight(double weight) {
		this.weight = weight;
	}

	public double getHeight() {
		return height;
	}

	public void setHeight(double height) {
		this.height = height;
	}

	public Gender getGender() {
		return gender;
	}

	public void setGender(Gender gender) {
		this.gender = gender;
	}

	public String getNationality() {
		return nationality;
	}

	public void setNationality(String nationality) {
		this.nationality = nationality;
	}

	public LocalDate getBirthdate() {
		return birthdate;
	}

	public void setBirthdate(String birthdate) {
		this.birthdate = LocalDate.parse(birthdate, formatter);
	}

	public double getRacketLength() {
		return racketLength;
	}

	public void setRacketLength(double racketLength) {
		this.racketLength = racketLength;
	}

	public int getWorldRanking() {
		return worldRanking;
	}

	public void setWorldRanking(int worldRanking) {
		this.worldRanking = worldRanking;
	}

	/**
	 * 
	 * This method is to compare age of a given another athlete with this athlete.
	 * To find the different in year between two LocalDate, use LocalDate
	 * dateBefore; LocalDate dateAfter; int year =
	 * ChronoUnit.YEARS.between(dateBefore, dateAfter);
	 * 
	 *not done yet
	 * 
	 * @param person - Another athlete to compare age with
	 */

	public void compareAge(Footballer person) {
		LocalDate bd1 = getBirthdate();
		LocalDate bd2 = person.getBirthdate();
		long diff = ChronoUnit.YEARS.between(bd1, bd2);
		if (diff < 0) {
			System.out.println(person.getName() + "is " + diff * -1 + " years older than " + getName());
		} else {
			System.out.println(getName() + "is " + diff + " years older than " + person.getName());
		}
	}
	
	public void compareAge(BadmintonPlayer person) {
		LocalDate bd1 = getBirthdate();
		LocalDate bd2 = person.getBirthdate();
		long diff = ChronoUnit.YEARS.between(bd1, bd2);
		if (diff < 0) {
			System.out.println(person.getName() + "is " + diff * -1 + " years older than " + getName());
		} else {
			System.out.println(getName() + "is " + diff + " years older than " + person.getName());
		}
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

}