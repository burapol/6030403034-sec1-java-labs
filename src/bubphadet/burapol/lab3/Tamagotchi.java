package bubphadet.burapol.lab3;
/**
 * 
 * Burapol Bubphadet 603040303-4 sec1
 * 
 * 
 *Tamagotchi is a program that simulates a digital pet where you have to take care of the pet.
 * 
 */
import java.util.Scanner;

public class Tamagotchi {

	private static Scanner sc;

	public static void main(String[] args) {
		sc = new Scanner(System.in);
		System.out.print("Enter your Tamagotchi info : ");
		int live = sc.nextInt();
		int food = sc.nextInt();
		int water = sc.nextInt();

		System.out.println("Your tamagotchi will live for " + live + " hours");
		System.out.println("It needs to be fed every " + food + " hours");
		System.out.println("It needs to be water every " + water + " hours");
		System.out.println("");

		int fedEvery = (live / food) - 1; //find times for feed to your tamagotchi
		int waterEvery = (live / water) - 1; //find times for give water to your tamagotchi

		if (water > food) {  //check times you need to water-feed
			int waterandfed = waterEvery;
			System.out.println("You need to water-feed: " + waterandfed + " times");
		} else if (water < food) {
			int waterandfed = fedEvery;
			System.out.println("You need to water-feed: " + waterandfed + " times");
		}
		System.out.println("You need to water: " + waterEvery + " times");
		System.out.println("You need to feed: " + fedEvery + " times");

	}

}
