package bubphadet.burapol.lab3;
/**
 * 
 * Burapol Bubphadet 603040303-4 sec1
 * 
 * 
 *  DiceMethodGame that produces the same result as DiceGame. 
 *  The program defines three methods, namely acceptInput(), genDiceRoll() and displayWiner() 
 *  described below. The program also contains two static String variable, namely humanGuest and computerScore 
 *  to be used in the program methods. Your main method 
 * 
 */
import java.util.Random;
import java.util.Scanner;

public class DiceMethodGame {

	static int humanGuest;
	static int computerScore;
	private static Scanner sc;

	public static void main(String[] args) {
		humanGuest = acceptInput(); //enter number function
		computerScore = genDiceRoll();// computer gen number function
		displayWinner(humanGuest,computerScore);//print winner
	}

	private static void displayWinner(int humanGuest, int computerScore) {  //function find winner 
		if (Math.abs(humanGuest - computerScore) <= 1 || Math.abs(humanGuest - computerScore) == 5) {
			System.out.print("You win.");
		} else {
			System.out.print("Computer wins.");
		}
		
	}

	private static int acceptInput() {  //function enter number from guest
		sc = new Scanner(System.in);
		System.out.print("Enter your guest : ");
		int guest = sc.nextInt();
		System.out.println("You have guested number : " + guest);
		return guest;
	}

	private static int genDiceRoll() {  //function find number from computer random
		Random randomno = new Random();
		int score = randomno.nextInt((6 - 1) + 1) + 1;
		System.out.println("Computer has rolled number : " + score);
		return score;
	}

}
