package bubphadet.burapol.lab3;
/**
 * 
 * Burapol Bubphadet 603040303-4 sec1
 * 
 * 
 * this program is dicegame. DiceGame that simulates a game of two players where one player (a human player) 
 * guests a score that another player (a computer player) will get from roll a dice. 
 * The computer player rolls a dice (for number between 1 to 6) for a score. 
 * If the guest number is the same or close to the score then the human player wins, 
 * if the guest is not the same nor close to the score then the computer player wins.
 * 
 */
import java.util.Random;
import java.util.Scanner;

class DiceGame {
	public static void main(String args[]) {
		Scanner sc = new Scanner(System.in);//scan input
		System.out.print("Enter your guest : ");//enter your number
		int guest = sc.nextInt();//input is interger
		if (guest <= 6 && guest >= 1) {  //check guest and score from computer for find winner.
			System.out.println("You have guested number : " + guest);
			Random randomno = new Random();//random number 1-6
			int score = randomno.nextInt((6 - 1) + 1) + 1;
			System.out.println("Computer has rolled number : " + score);
			if (Math.abs(guest - score) <= 1 || Math.abs(guest - score) == 5) {
				System.out.print("You win.");//if guest win print this
			} else {
				System.out.print("Computer wins.");//if computer win print this
			}
			
		} else {
			System.err.print("Incorrect number. Only 1-6 can be entered.");//if your enter number out of range print this err
		}
		

		sc.close();
	}
}