package bubphadet.burapol.lab8;

import java.awt.geom.Ellipse2D;
/**
 * Ball is class extends from Ellipse2D.Double. for create ball 
 * 
 *
 * @author Burapol Bubphadet
 * @version 1.0
 * @since 2018-03-26
 * 
 * 
 * 
 */

public class Ball extends Ellipse2D.Double {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public Ball(int x, int y, int r1, int r2){ //constructor use super
		super(x,y,r1,r2);
	}
	
	

	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

}
