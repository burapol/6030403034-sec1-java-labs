package bubphadet.burapol.lab8;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import javax.swing.JPanel;

/**
 * WrapAroundBall is class extends from JPanel and implements from Runnable . 
 * The class will create a ball, which is moving the ball around.
 * 
 *
 * @author Burapol Bubphadet
 * @version 1.0
 * @since 2018-03-26
 * 
 * 
 * 
 */
public class WrapAroundBall extends JPanel implements Runnable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	MovingBall ball, ball2, ball3, ball4, ball5;
	private int r;
	{
		r = 20;
	}
	Thread running;

	public WrapAroundBall() {
		setBackground(Color.BLUE); //set object and set background color
		ball = new MovingBall(60, 0, r, 1, 1);
		ball2 = new MovingBall(0, 60, r, 1, 1);
		ball3 = new MovingBall(0, 0, r, 1, 1);
		ball4 = new MovingBall(200, TestWrapAroundBall.HEIGHT, r, 1, 1);
		ball5 = new MovingBall(TestWrapAroundBall.WIDTH, 100, r, 1, 1);

		running = new Thread(this);
		running.start();
	}

	@Override
	public void run() {
		// TODO Auto-generated method stub
		while (true) {

			ball.x = ball.x + 0; //set X,Y axis movement.
			ball.y = ball.y + ball.getVelY();

			ball2.x = ball2.x + ball2.getVelX();
			ball2.y = ball2.y + 0;

			ball3.x = ball3.x + ball.getVelX();
			ball3.y = ball3.y + ball3.getVelY();

			ball4.x = ball4.x + 0;
			ball4.y = ball4.y - ball4.getVelY();

			ball5.x = ball5.x - ball5.getVelX();
			ball5.y = ball5.y + 0;

			if (ball.y > TestWrapAroundBall.HEIGHT) { //create moving loop
				ball.y = -r * 2;
			} else if (ball2.x > TestWrapAroundBall.WIDTH) {
				ball2.x = -r * 2;
			} else if (ball3.x > TestWrapAroundBall.WIDTH) {
				ball3.x = -r * 2;
			} else if (ball3.y > TestWrapAroundBall.HEIGHT) {
				ball3.y = -r * 2;
			} else if (ball4.y + 2 * r < 0) {
				ball4.y = TestWrapAroundBall.HEIGHT + r * 2;
			} else if (ball5.x + 2 * r < 0) {
				ball5.x = TestWrapAroundBall.WIDTH + r * 2;
			}

			repaint();
			this.getToolkit().sync();

			// Delay
			try {

				Thread.sleep(15);
			} catch (InterruptedException ex) {
				System.err.println(ex.getStackTrace());
			}
		}
	}

	public void paintComponent(Graphics g) { //paint object

		super.paintComponent(g);

		Graphics2D g2d = (Graphics2D) g;
		g2d.setColor(Color.YELLOW);
		g2d.fill(ball);

		g2d.setColor(Color.RED);
		g2d.fill(ball2);

		g2d.setColor(Color.GREEN);
		g2d.fill(ball3);

		g2d.setColor(Color.BLACK);
		g2d.fill(ball4);

		g2d.setColor(Color.WHITE);
		g2d.fill(ball5);

	}
}
