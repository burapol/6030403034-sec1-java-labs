package bubphadet.burapol.lab8;

import java.awt.Dimension;
import java.awt.Toolkit;

import javax.swing.JFrame;

/**
 * PongGameGUI is class extends from SimpleGameWindow . 
 * The class will set pane.
 * 
 *
 * @author Burapol Bubphadet
 * @version 1.0
 * @since 2018-03-26
 * 
 * 
 * 
 */

public class PongGameGUI extends SimpleGameWindow {
	private static final long serialVersionUID = 1L;

	static final int WIDTH = 650;
	static final int HEIGHT = 500;

	public PongGameGUI(String string) {
		super(string);
	}

	public void setFrameFeatures() {
		setSize(WIDTH, HEIGHT);
		Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
		int w = getSize().width;
		int h = getSize().height;
		int x = (dim.width - w) / 2;
		int y = (dim.height - h) / 2;
		setLocation(x, y);
		setVisible(true);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}

	public void addComponents() {
		PongPanel panel = new PongPanel();
		setContentPane(panel);
	}

}
