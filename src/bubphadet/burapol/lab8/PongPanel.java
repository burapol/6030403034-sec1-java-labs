package bubphadet.burapol.lab8;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.geom.Line2D;

import javax.swing.JPanel;

/**
 * PongPanel is class extends from JPanel . 
 * The class will create panel and component.
 * 
 *
 * @author Burapol Bubphadet
 * @version 1.0
 * @since 2018-03-26
 * 
 * 
 * 
 */
public class PongPanel extends JPanel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private PongPaddle LeftPongPaddle;
	private PongPaddle RightPongPaddle;
	private int LeftScore, RightScore;
	{
		LeftScore = 0;
		RightScore = 0;
	}
	private Ball Ball;
	private Line2D.Double line1, line2, line3;

	public PongPanel() {
		setBackground(Color.BLACK); //create components and set background color.
		LeftPongPaddle = new PongPaddle(0, PongGameGUI.HEIGHT / 2 - 40, 15, 80);
		RightPongPaddle = new PongPaddle(PongGameGUI.WIDTH - 30, PongGameGUI.HEIGHT / 2 - 40, 15, 80);
		Ball = new Ball(PongGameGUI.WIDTH / 2 - 20, PongGameGUI.HEIGHT / 2 - 20, 40, 40);
		line1 = new Line2D.Double(PongGameGUI.WIDTH / 2, 0, PongGameGUI.WIDTH / 2, PongGameGUI.HEIGHT);
		line2 = new Line2D.Double(15, 0, 15, PongGameGUI.HEIGHT);
		line3 = new Line2D.Double(PongGameGUI.WIDTH - 30, 0, PongGameGUI.WIDTH - 30, PongGameGUI.HEIGHT);

	}

	public void paintComponent(Graphics g) { //method for paint
		super.paintComponent(g);

		Graphics2D g2d = (Graphics2D) g;

		g2d.setColor(Color.WHITE);
		g2d.fill(LeftPongPaddle);
		;

		g2d.fill(RightPongPaddle);

		g2d.fill(Ball);

		g2d.draw(line1);
		g2d.draw(line2);
		g2d.draw(line3);

		Font font = new Font("Serif", Font.BOLD, 48);
		g2d.setFont(font);
		String leftscore = Integer.toString(LeftScore);
		g2d.drawString(leftscore, 150, 80);

		String rightscore = Integer.toString(RightScore);
		g2d.drawString(rightscore, 450, 80);

	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

}
