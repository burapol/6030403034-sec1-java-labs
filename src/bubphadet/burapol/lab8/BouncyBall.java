package bubphadet.burapol.lab8;

import java.awt.Dimension;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.geom.Rectangle2D;
import javax.swing.JPanel;
/**
 * BouncyBall is class extends from JPanel and implements from Runnable . 
 * The class will create a ball that will bounce of a wall
 * 
 *
 * @author Burapol Bubphadet
 * @version 1.0
 * @since 2018-03-26
 * 
 * 
 * 
 */
public class BouncyBall extends JPanel implements Runnable {
	private static final long serialVersionUID = 1L;
	Thread running;
	MovingBall ball;
	Rectangle2D.Double box;
	int r = 20;

	public BouncyBall() {
		setBackground(Color.GREEN); //set screen color
		setPreferredSize(new Dimension(TestBouncyBall.WIDTH, TestBouncyBall.HEIGHT)); //set object
		box = new Rectangle2D.Double(TestBouncyBall.WIDTH, TestBouncyBall.HEIGHT, 2 * r, 2 * r);
		ball = new MovingBall(TestBouncyBall.WIDTH / 2, TestBouncyBall.HEIGHT / 2, r, 2, 2);
		running = new Thread(this);
		running.start();
	}

	int ballVelX = 1;
	int ballVelY = 1;

	@Override
	public void run() {
		while (true) {
			
			int ballVelX = ball.getVelX();
			int ballVelY = ball.getVelY();
			
			ball.x = ball.x + ballVelX;
			ball.y = ball.y + ballVelY;
			
			if (ball.x + 2*r > TestBouncyBall.WIDTH || ball.x + 2*r < 0) {   // for bounce of a wall
				ball.setVelX(-ball.getVelX());
			} else if(ball.y + 2*r > TestBouncyBall.HEIGHT || ball.y + 2*r < 0) {
				ball.setVelY(-ball.getVelY());
			}

			repaint();
			this.getToolkit().sync();

			// Delay
			try {
				Thread.sleep(20);
			} catch (InterruptedException ex) {
				System.err.println(ex.getStackTrace());
			}
		}
	}

	public void paintComponent(Graphics g) {
		super.paintComponent(g);

		Graphics2D g2d = (Graphics2D) g;
		g2d.setColor(Color.YELLOW); //fill ball and set ball color
		g2d.fill(ball);
	}
}
