package bubphadet.burapol.lab8;

/**
 * MovingBall is class extends from Ball . 
 * The class will create a MovingBall(have speedx,y axis).
 * 
 *
 * @author Burapol Bubphadet
 * @version 1.0
 * @since 2018-03-26
 * 
 * 
 * 
 */
public class MovingBall extends Ball {

	private static final long serialVersionUID = 1L;
	private int velX = 1, velY = 1; //speed x,y axis

	public int getVelX() {
		return velX;
	}

	public void setVelX(int velX) {
		this.velX = velX;
	}

	public int getVelY() {
		return velY;
	}

	public void setVelY(int velY) {
		this.velY = velY;
	}

	public MovingBall(int x, int y, int r, int velX, int velY) { //MovingBall constructor
		super(x, y, r * 2, r * 2);
		this.velX = velX;
		this.velY = velY;
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

}
