package bubphadet.burapol.lab8;

import java.awt.Dimension;
import java.awt.Toolkit;

import javax.swing.JFrame;
import javax.swing.SwingUtilities;


/**
 * TestWrapAroundBall is class extends from SimpleGameWindow . 
 * The class will create WrapAroundBall UI.
 * 
 *
 * @author Burapol Bubphadet
 * @version 1.0
 * @since 2018-03-26
 * 
 * 
 * 
 */
public class TestWrapAroundBall extends SimpleGameWindow {

	public TestWrapAroundBall(String string) {
		super(string);
		// TODO Auto-generated constructor stub
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	static final int WIDTH = 650;
	static final int HEIGHT = 500;
	

	protected void setFrameFeatures() {
		setSize(WIDTH,  HEIGHT);
		Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
		int w = getSize().width;
		int h = getSize().height;
		int x = (dim.width - w)/2;
		int y = (dim.height - h)/2;
		setLocation(x,y);
	    setVisible(true);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}
	
	protected void  addComponents() {
		WrapAroundBall canvas = new WrapAroundBall();
		setContentPane(canvas);
	}
	
	public static void createAndShowGUI() {
		TestWrapAroundBall window = new TestWrapAroundBall("Test WrapAround Ball");
		window.addComponents();
		window.setFrameFeatures();
	}
	
	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});
	}

}
