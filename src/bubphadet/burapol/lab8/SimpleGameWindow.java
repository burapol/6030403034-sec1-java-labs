package bubphadet.burapol.lab8;

import javax.swing.JFrame;
import javax.swing.SwingUtilities;

/**
 * SimpleGameWindow is class extends from JFrame . 
 * The class will create frame.
 * 
 *
 * @author Burapol Bubphadet
 * @version 1.0
 * @since 2018-03-26
 * 
 * 
 * 
 */

public class SimpleGameWindow extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public SimpleGameWindow(String string) {
		super(string);
	}

	public static void createAndShowGUI() {
		PongGameGUI window = new PongGameGUI("Pong Game Window");
		window.addComponents();
		window.setFrameFeatures();
	}
	
	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});
	}
	
}