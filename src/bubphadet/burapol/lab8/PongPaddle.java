package bubphadet.burapol.lab8;

import java.awt.geom.Rectangle2D;

/**
 * PongPaddle is class extends from Rectangle2D.Double . 
 * The class will create paddle.
 * 
 *
 * @author Burapol Bubphadet
 * @version 1.0
 * @since 2018-03-26
 * 
 * 
 * 
 */

public class PongPaddle extends Rectangle2D.Double {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public PongPaddle(int x,int y,int w,int h){
		super(x,y,w,h);
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

}
