package bubphadet.burapol.lab12;

import java.time.format.DateTimeParseException;

import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;

import bubphadet.burapol.lab5.Athlete;
import bubphadet.burapol.lab5.Gender;
/**
 * AthleteFormV11 which extends from AthleteFormV10
 * 
 *
 * @author Burapol Bubphadet
 * @version 1.0
 * @since 2018-05-15
 * 
 */
public class AthleteFormV11 extends AthleteFormV10 {
	protected String name;


	public AthleteFormV11(String title) {
		super(title);
		// TODO Auto-generated constructor stub
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public void addAthlete() {
		if (genderButton == "Male") {
			gender = Gender.MALE;
		} else {
			gender = Gender.FEMALE;
		}
		try {
			if (nameField.getText().equals("")) {
				name = null;
				JOptionPane.showMessageDialog(this, "Please enter athlete name");
			} else {
				name = nameField.getText();
			}
		} catch (Exception ex) {
			ex.printStackTrace(System.err);
		}
		try {
			if (birthdateField.getText().length() == 0 || !(birthdateField.getText().matches("\\d{2}/\\d{2}/\\d{4}"))) {
				throw new DateTimeParseException("Please enter date in the format dd/MM/yyyy  such as 02/02/2000",
						birthdateField.getText(), 0);
			} else {
			}
		} catch (DateTimeParseException e) {
			JOptionPane.showMessageDialog(this, e.getMessage());
		}

		try {
			if (weightField.getText().length() == 0 && !weightField.getText().matches("[0-9]+")) {
				throw new NumberFormatException("Please enter weight as double value");
			} else {
			}
		} catch (NumberFormatException e) {
			JOptionPane.showMessageDialog(this, e.getMessage());
		}

		try {
			if (heightField.getText().length() == 0 && !heightField.getText().matches("[0-9]+")) {
				throw new NumberFormatException("Please enter height as double value");
			} else {
			}
		} catch (NumberFormatException e) {
			JOptionPane.showMessageDialog(this, e.getMessage());
		}

		if (nameField.getText().length() != 0 || nationalityField.getText().length() != 0) {
			athlete_List.add(new Athlete(name , Double.parseDouble(weightField.getText()),
					Double.parseDouble(heightField.getText()), gender, nationalityField.getText(),
					birthdateField.getText()));
		}
		System.out.println(athlete_List);
	}
	
	public static void createAndShowGUI() {
		AthleteFormV11 AthleteForm11 = new AthleteFormV11("Athlete Form V11");
		AthleteForm11.addComponents();
		AthleteForm11.setFrameFeatures();
	}

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});
	}
}
