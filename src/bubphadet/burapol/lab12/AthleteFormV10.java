package bubphadet.burapol.lab12;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.text.SimpleDateFormat;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;
/**
 * AthleteFormV10 which extends from AthleteFormV9
 * 
 *
 * @author Burapol Bubphadet
 * @version 1.0
 * @since 2018-05-15
 * 
 */
public class AthleteFormV10 extends AthleteFormV9 {
	SimpleDateFormat formatter;
	protected String msg_line, msg;

	public AthleteFormV10(String title) {
		super(title);
		// TODO Auto-generated constructor stub
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public void save_filechooser() {
		FileWriter file_writer;
		String text_flie;
		save_filechooser = new JFileChooser();
		save_filechooser.setDialogType(JFileChooser.SAVE_DIALOG);
		int result = save_filechooser.showSaveDialog(null); 
		if (result == JFileChooser.APPROVE_OPTION) { 
			File selected = save_filechooser.getSelectedFile();
			try {
				file_writer = new FileWriter(selected.getPath());
				text_flie = "";
				for (int i = 0; i < athlete_List.size(); i++) {
					text_flie += (i+1) + ". " + athlete_List.get(i) + "\n";
				}
				file_writer.write(text_flie);
				file_writer.flush(); 
			} catch (Exception ex) {
	    		ex.printStackTrace(System.err);
			}
		} else {
			JOptionPane.showMessageDialog(this, "Saving command cancelled by user.");
		}
	}
	
	protected void open_filechooser() {
		BufferedReader reader;
		open_filechooser = new JFileChooser();
		open_filechooser.setDialogType(JFileChooser.OPEN_DIALOG); 
		open_filechooser.setDialogTitle("Open");
		int selectedButton = open_filechooser.showDialog(null, "Open");
		if (selectedButton == JFileChooser.APPROVE_OPTION) { 
			try {
				File selected = open_filechooser.getSelectedFile();
				reader = new BufferedReader(new FileReader(selected.getPath()));
				msg_line = "";
				msg = "";
				while ((msg_line = reader.readLine()) != null) {
					msg += (msg_line+"\n"); 
				}
				JOptionPane.showMessageDialog(null, msg);
				if (reader != null) {
					reader.close();
				}
			} catch (Exception ex) {
	    		ex.printStackTrace(System.err);
			}
		} else { 
			JOptionPane.showMessageDialog(this, "Opening command cancelled by user.");
		}
	}

	public void addListeners() {
		super.addListeners();
		open_Item.addActionListener(this);
		open_Item.removeActionListener(open_Item.getActionListeners()[0]);
		save_Item.addActionListener(this);
		save_Item.removeActionListener(save_Item.getActionListeners()[0]);

	}
	
	public static void createAndShowGUI() {
		AthleteFormV10 AthleteForm10 = new AthleteFormV10("Athlete Form V10");
		AthleteForm10.addComponents();
		AthleteForm10.setFrameFeatures();
	}

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});
	}

}
