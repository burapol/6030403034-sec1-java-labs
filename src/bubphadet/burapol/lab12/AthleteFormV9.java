package bubphadet.burapol.lab12;

import java.awt.event.ActionEvent;



import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;

import bubphadet.burapol.lab11.AthleteFormV8;

/**
 * AthleteFormV9 which extends from AthleteFormV8
 * 
 *
 * @author Burapol Bubphadet
 * @version 1.0
 * @since 2018-05-15
 * 
 */
public class AthleteFormV9 extends AthleteFormV8 {
	protected String search;

	public AthleteFormV9(String title) {
		super(title);
		// TODO Auto-generated constructor stub
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	protected void addComponents() {
		super.addComponents();
	}
	
	public String search_item() {
		search = JOptionPane.showInputDialog("Please enter the athlete name ");
		for (int i = 0; i < athlete_List.size(); i++) {
			String name = this.athlete_List.get(i).getName();
			if(name.equals(search)) {
				return athlete_List.get(i).toString();
			}
		}
		return null;
	}
	
	public void print_search() {
		String seach = search_item();
		if (seach != null) {
			JOptionPane.showMessageDialog(null,seach);
		} else if (seach == null) {
			JOptionPane.showMessageDialog(null,search + " is not found. ");
		}
	}
	
	public void remove_athlete() {
		String remove_search = JOptionPane.showInputDialog("Please enter the athlete name to remove ");
		for (int i = 0; i < athlete_List.size(); i++) {
			String name = this.athlete_List.get(i).getName();
			if(name.equals(remove_search)) {
				athlete_List.remove(i);
			}
		}
	}
	
	@Override
	public void actionPerformed(ActionEvent event) {
		super.actionPerformed(event);
		Object src = event.getSource();
		if (src == search_Item) {
			print_search();
		} else if(src == remove_Item) {
			remove_athlete();
		}
	}
	
	public void addListeners() {
		super.addListeners();
		search_Item.addActionListener(this);
		remove_Item.addActionListener(this);
	}

	public static void createAndShowGUI() {
		AthleteFormV9 AthleteForm9 = new AthleteFormV9("Athlete Form V9");
		AthleteForm9.addComponents();
		AthleteForm9.setFrameFeatures();
	}

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});
	}

}